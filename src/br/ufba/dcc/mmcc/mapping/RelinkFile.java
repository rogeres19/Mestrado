package br.ufba.dcc.mmcc.mapping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.ufba.dcc.mmcc.util.List;

public class RelinkFile extends LeitorMetaModelo{

	private ArrayList<Integer> idBugs= new ArrayList<Integer>();
	private ArrayList<Integer> idCommitsBugs=new ArrayList<Integer>();
	private ArrayList<String> comments=new ArrayList<String>();
	private List<Integer> listBugs = new List<Integer>(); 
	private Map<Integer, String> mapIdCommitComment = new HashMap<Integer, String>();
	private Map<Integer, Integer> mapIdCommitBug = new HashMap<Integer, Integer>();	
	
	RelinkFile(String path) throws IOException {
		super(path);
		readLine();
	}

@Override
	protected void readLine() {
		String line = null;
		int i=0;		
		try {
			line = readFile.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		while(line!=null){	
			String[] chunk = line.split("\t");
			if(!listBugs.contains(Integer.parseInt(chunk[0]))){//check bugs exist
				this.listBugs.add(Integer.parseInt(chunk[0]));
			}
			this.idBugs.add(Integer.parseInt(chunk[0]));
			this.idCommitsBugs.add(Integer.parseInt(chunk[8]));
			this.comments.add(chunk[10]);			
			this.mapIdCommitComment.put(Integer.parseInt(chunk[8]),chunk[10]);
			this.mapIdCommitBug.put(Integer.parseInt(chunk[8]),Integer.parseInt(chunk[0]));			
			i++;
			try {
				line = readFile.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
	}

	public ArrayList<Integer> getListBugs(){
		return this.listBugs;
	}
	
	public ArrayList<Integer> getcommitsBugs(){
		return this.idCommitsBugs;
	}
	
	public ArrayList<Integer> getIdBugs(){
		return this.idBugs;
	}
	
	public ArrayList<String> getComments(){
		return this.comments;
	}
	
	public Map<Integer,String> getMapIdCommitComment(){
		return this.mapIdCommitComment;
	}
	
	public Map<Integer,Integer> getMapIdCommitBug(){
		return this.mapIdCommitBug;
	}

}
