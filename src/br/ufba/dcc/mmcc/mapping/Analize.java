package br.ufba.dcc.mmcc.mapping;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

	public class Analize {	
		private ArrayList<String> content= new ArrayList<String>();
		private Map<Integer,String> mapIdBadSmell = new HashMap<Integer,String>();


		Analize(VersionMapping bugMapping,BadSmellsMapping badSmellsMapping) {
			definingBadSmellsMapping();
			correlate(bugMapping,badSmellsMapping);
			correlateInverso(bugMapping,badSmellsMapping);
		}
		
		private void definingBadSmellsMapping(){
			mapIdBadSmell.put(0,"dataclass");
			mapIdBadSmell.put(1,"dataclumps");
			mapIdBadSmell.put(2,"featureenvy");
			mapIdBadSmell.put(3,"godclass");
			mapIdBadSmell.put(4,"messagechains");		
			mapIdBadSmell.put(5,"scrizophrenicclass");		
			mapIdBadSmell.put(6,"traditionbreaker");
		}	
		
	private void correlate(VersionMapping bugMapping, BadSmellsMapping badSmellsMapping){		
		ArrayList<ArrayList<ArrayList<String>>> badSmellsClassesVersion= badSmellsMapping.getBadSmellsClassesVersion();
		ArrayList<ArrayList<String>> bugsClassesVersion= bugMapping.getListClassVersion();
		content.add("Version\t"
		+"Class BS&Bug\t"
		+mapIdBadSmell.get(0)+"\tBUG(true)\tBUG(false)\t"
		+mapIdBadSmell.get(1)+"\tBUG(true)\tBUG(false)\t"
		+mapIdBadSmell.get(2)+"\tBUG(true)\tBUG(false)\t"
		+mapIdBadSmell.get(3)+"\tBUG(true)\tBUG(false)\t"
		+mapIdBadSmell.get(4)+"\tBUG(true)\tBUG(false)\t"
		+mapIdBadSmell.get(5)+"\tBUG(true)\tBUG(false)\t"
		+mapIdBadSmell.get(6)+"\tBUG(true)\tBUG(false)");
		int[] totalBugs = {0,0,0,0,0,0,0};
		int[] totalNoBugs = {0,0,0,0,0,0,0};
		String totalContent= new String();
		for(int version=0;version<bugMapping.getListSvnVersions().size();version++){			
			ArrayList<String> badsmellsBugs= new ArrayList<String>();			
			System.out.println("bugs: version "+(bugMapping.getListSvnVersions().size()-version)+ bugsClassesVersion.get(version));
			//content.add("bugs: version "+((bugMapping.getListSvnVersions().size()-version)-1)+ bugsClassesVersion.get(version));
			String temp= new String();			
			for(int idBadSmell=0; idBadSmell<badSmellsClassesVersion.get(version).size(); idBadSmell++){// type of bad smell
				int n=0;
				for(int j=0;j<bugsClassesVersion.get(version).size();j++){//bugs class version
					for(int i =0;i<badSmellsClassesVersion.get(version).get(idBadSmell).size();i++){ //bad smells class version
						if(bugsClassesVersion.get(version).get(j).contains(badSmellsClassesVersion.get(version).get(idBadSmell).get(i))){
							if(!badsmellsBugs.contains(badSmellsClassesVersion.get(version).get(idBadSmell).get(i)))
								badsmellsBugs.add(badSmellsClassesVersion.get(version).get(idBadSmell).get(i));						
							System.out.println(mapIdBadSmell.get(idBadSmell)+" - smells classes "+badSmellsClassesVersion.get(version).get(idBadSmell).get(i));
							n=n+1;							
						}
					}					
				}
				temp=temp+badSmellsClassesVersion.get(version).get(idBadSmell).size()+"\t"+n+"\t"+(badSmellsClassesVersion.get(version).get(idBadSmell).size()-n+"\t");
				totalBugs[idBadSmell]=totalBugs[idBadSmell]+n;
				totalNoBugs[idBadSmell]=totalNoBugs[idBadSmell]+(badSmellsClassesVersion.get(version).get(idBadSmell).size()-n);
				if(version==bugMapping.getListSvnVersions().size()-1)
					totalContent=totalContent+mapIdBadSmell.get(idBadSmell)+"\t"+totalBugs[idBadSmell]+"\t"+totalNoBugs[idBadSmell]+"\n";
				System.out.println(mapIdBadSmell.get(idBadSmell)+" - CLASSES:"+badSmellsClassesVersion.get(version).get(idBadSmell).size()+" - COM BUGS: "+n);
				//content.add("iguais: "+n);				
			}
			content.add(((bugMapping.getListSvnVersions().size()-version)-1)+"\t"+badsmellsBugs.size()+"\t"+temp);
			System.out.println("Classes com bad Smells e bugs: "+badsmellsBugs.size());
			//content.add("Classes com bad Smells e bugs: "+badsmellsBugs.size());
		}
		content.add("\n"+totalContent);
		System.out.println(bugsClassesVersion.get(0));
		//putSubArrayToContent(bugsClassesVersion.get(0));
	}
	
	private void correlateInverso(VersionMapping bugMapping, BadSmellsMapping badSmellsMapping){		
		ArrayList<ArrayList<ArrayList<String>>> badSmellsClassesVersion= badSmellsMapping.getBadSmellsClassesVersion();
		ArrayList<ArrayList<String>> bugsClassesVersion= bugMapping.getListClassVersion();
		content.add("VersionInverso\t"
		+"Class BS&Bug\t"
		+mapIdBadSmell.get(0)+"\tBUG(true)\tBUG(false)\t"
		+mapIdBadSmell.get(1)+"\tBUG(true)\tBUG(false)\t"
		+mapIdBadSmell.get(2)+"\tBUG(true)\tBUG(false)\t"
		+mapIdBadSmell.get(3)+"\tBUG(true)\tBUG(false)\t"
		+mapIdBadSmell.get(4)+"\tBUG(true)\tBUG(false)\t"
		+mapIdBadSmell.get(5)+"\tBUG(true)\tBUG(false)\t"
		+mapIdBadSmell.get(6)+"\tBUG(true)\tBUG(false)");
		int[] totalBugs = {0,0,0,0,0,0,0};
		int[] totalNoBugs = {0,0,0,0,0,0,0};
		String totalContent= new String();
		for(int version=0;version<bugMapping.getListSvnVersions().size();version++){
			if(version+1==bugMapping.getListSvnVersions().size())
				break;
			ArrayList<String> badsmellsBugs= new ArrayList<String>();			
			System.out.println("bugs: version "+(bugMapping.getListSvnVersions().size()-version)+ bugsClassesVersion.get(version));
			//content.add("bugs: version "+((bugMapping.getListSvnVersions().size()-version)-1)+ bugsClassesVersion.get(version));
			String temp= new String();
			for(int idBadSmell=0; idBadSmell<badSmellsClassesVersion.get(version).size(); idBadSmell++){// type of bad smell
				int n=0;				
				for(int j=0;j<bugsClassesVersion.get(version+1).size();j++){//bugs class version
					for(int i =0;i<badSmellsClassesVersion.get(version).get(idBadSmell).size();i++){ //bad smells class version
						if(bugsClassesVersion.get(version+1).get(j).contains(badSmellsClassesVersion.get(version).get(idBadSmell).get(i))){
							if(!badsmellsBugs.contains(badSmellsClassesVersion.get(version).get(idBadSmell).get(i)))
								badsmellsBugs.add(badSmellsClassesVersion.get(version).get(idBadSmell).get(i));
						
							System.out.println(mapIdBadSmell.get(idBadSmell)+" - smells classes "+badSmellsClassesVersion.get(version).get(idBadSmell).get(i));
							n=n+1;							
						}
					}
				}
				temp=temp+badSmellsClassesVersion.get(version).get(idBadSmell).size()+"\t"+n+"\t"+(badSmellsClassesVersion.get(version).get(idBadSmell).size()-n+"\t");
				totalBugs[idBadSmell]=totalBugs[idBadSmell]+n;
				totalNoBugs[idBadSmell]=totalNoBugs[idBadSmell]+(badSmellsClassesVersion.get(version).get(idBadSmell).size()-n);
				if(version==bugMapping.getListSvnVersions().size()-1)
					totalContent=totalContent+mapIdBadSmell.get(idBadSmell)+"\t"+totalBugs[idBadSmell]+"\t"+totalNoBugs[idBadSmell]+"\n";
				System.out.println(mapIdBadSmell.get(idBadSmell)+" - CLASSES:"+badSmellsClassesVersion.get(version).get(idBadSmell).size()+" - COM BUGS: "+n);
				//content.add("iguais: "+n);				
			}
			content.add(((bugMapping.getListSvnVersions().size()-version)-1)+"\t"+badsmellsBugs.size()+"\t"+temp);
			System.out.println("Classes com bad Smells e bugs: "+badsmellsBugs.size());
			//content.add("Classes com bad Smells e bugs: "+badsmellsBugs.size());
		}
		content.add("\n"+totalContent);
		System.out.println(bugsClassesVersion.get(0));
		//putSubArrayToContent(bugsClassesVersion.get(0));
	}
	
	public ArrayList<String> getContent(){
		Collections.reverse(content);
		return this.content;		
	}
	
	private void putSubArrayToContent(ArrayList<?> subArray){//process  sub array
		for(int index=0;index<subArray.size();index++){
			this.content.add(subArray.get(index).toString());			
		}		
	}
	
}
