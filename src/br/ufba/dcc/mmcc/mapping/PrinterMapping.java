package br.ufba.dcc.mmcc.mapping;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class PrinterMapping {
	String path;
	String filename="mapping_bugs_versions_repeat.txt";
	//"mapping_bugs_versions.txt"
	
	public PrinterMapping(String path,ArrayList<String> content) throws IOException {
		printer(path,content);		
	}
	
	public void printer(String path,ArrayList<String> content) throws IOException{
		this.path=path;
		Path file = Paths.get(path);
		Files.write(file, content, Charset.forName("UTF-8"));
	}

}
