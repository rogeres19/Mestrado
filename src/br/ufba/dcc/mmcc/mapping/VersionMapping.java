package br.ufba.dcc.mmcc.mapping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

import br.ufba.dcc.mmcc.util.List;

public class VersionMapping {
	
	private SvnFile svn;
	private RelinkFile relink;
	private ArrayList<ArrayList<String>> listClassVersion= new ArrayList<ArrayList<String>>();	
	private ArrayList<ArrayList<Integer>>listBugsVersions= new  ArrayList<ArrayList<Integer>>();
	private ArrayList<ArrayList<Integer>>listIdCommitsBugsVersions= new ArrayList<ArrayList<Integer>>();	
	private ArrayList<String> listSvnVersions;
	private ArrayList<String> content= new ArrayList<String>();	
	//private ArrayList<String> listClassCommits=new ArrayList<ArrayList<String>();
	
	VersionMapping(SvnFile svn,RelinkFile relink){// constructor method
		this.svn=svn;
		this.relink=relink;
		listSvnVersions=svn.getversions();
		joinSvnRelink();
		designContent();
	}
	
	
	private void joinSvnRelink(){		
		ArrayList<Integer> idCommitsVersions=svn.getIdCommitsVersions();				
		ArrayList<Integer> idCommitsBugs=relink.getcommitsBugs();	
		ArrayList<Integer> idBugs=relink.getIdBugs();		
		Map<Integer,String> mapIdcommitBugComment = relink.getMapIdCommitComment();
		Map<Integer,Integer> mapIdcommitBug = relink.getMapIdCommitBug();
		int n=0;
		int i=0;
			
		Collections.sort(idCommitsBugs);//order ascending  by id bug commits
		Collections.reverse(idCommitsBugs);//reverse vector to descending
			
	    while(i<(idCommitsBugs.size()-1)){	///while index < size of array bug commits or 47<(48-1)=47
	    	List<Integer> bugsVersion=new List<Integer>();// bugs list
	    	ArrayList<String>listCommentsVersion= new ArrayList<String>();// list for version comments 
	    	ArrayList<Integer> idCommitsBugsVersion=new ArrayList<Integer>();//list for version commitsbugs
	    	
	    	while(i<(idCommitsBugs.size()-1) && idCommitsBugs.get(i)>idCommitsVersions.get(n)){// while commit value > commit of version print commit
	    		if(!idCommitsBugsVersion.contains(idCommitsBugs.get(i))){
					idCommitsBugsVersion.add(idCommitsBugs.get(i));
					listCommentsVersion.add(mapIdcommitBugComment.get(idCommitsBugs.get(i)));//get comment and put in the list
					if(!bugsVersion.contains(mapIdcommitBug.get(idCommitsBugs.get(i)))){//add new bug on bugs version if it not exist yet
						bugsVersion.add(mapIdcommitBug.get(idCommitsBugs.get(i)));//add idbugs
					}
	    		}
	    		i++ ;
			}	    	
	    	listClassVersion.add(getListClass(listCommentsVersion));//add list of comments on versioned list
	    	System.out.println(idCommitsBugsVersion);
	    	listBugsVersions.add(bugsVersion);//add list of bugs on list version
	    	listIdCommitsBugsVersions.add(idCommitsBugsVersion);//add list of commmits on list of committs per version 
	    	  	
			//this conditions control n(versions) for variable not explode
	    	if(n<(idCommitsVersions.size()-1)){				
				n++;
			}
			else{
				i=idCommitsBugs.size();
			}					
	   }//end first while			    			    
	}
	
	private ArrayList<String> getListClass(ArrayList<String> listCommentsVersion){
			List<String> listClassVersion= new List<String>();			
	        for(int i=0; i<listCommentsVersion.size();i++){
	    		ArrayList<String> classesCommit= getClassesCommit(listCommentsVersion.get(i));
	    		ArrayList<String> pack= new ArrayList<String>();
	    		int n=0;
	    		if(ignoreCommits(classesCommit))
	    			continue;
	    		for(int j=0;j<classesCommit.size();j++){
	    			if(classesCommit.get(j).contains(".java")  && !listClassVersion.contains(classesCommit.get(j).replace(" ",""))){	        				
        				listClassVersion.add(classesCommit.get(j).replace(" ",""));
        				n++;       				
        				System.out.println("commit"+i+classesCommit.get(j));
        				String [] pac =classesCommit.get(j).split("/");
        				if(!pack.contains(pac[pac.length-2])){
        					System.out.println("pack: "+pac[pac.length-2]);
        					pack.add(pac[pac.length-2]);
        				}
        			}	    			
	    		}
	    		System.out.println("commit"+i+" All = "+n);
	    		System.out.println("package: "+pack.size());
	    	
	        }
	        
	         	
	        	
			return listClassVersion;
	}
	
	private boolean ignoreCommits(ArrayList<String> classesCommit){
		ArrayList<String> pack= new ArrayList<String>();
		for(int j=0;j<classesCommit.size();j++){
			if(classesCommit.get(j).contains(".java")){	        				
				String [] pac =classesCommit.get(j).split("/");
				if(!pack.contains(pac[pac.length-2])){
					pack.add(pac[pac.length-2]);
				}
			}	    			
		}
		if(pack.size()>10)
			return true;
		else		
			return false;		
	}
	
	public ArrayList<String> getClassesCommit(String comment){
		String[] listClass=comment.split("Modified :");//split modified elements
		ArrayList<String> classesCommit= new ArrayList<String>();
		for(int i=0;i<listClass.length;i++ ){			
			String[] listDeleted=listClass[i].split("Deleted : ");
			if(listDeleted.length>1){
				for(int d=0;d<listDeleted.length;d++){					
					String[] listAdded=listDeleted[d].split("Added : ");
					if(listAdded.length>1)
						classesCommit.add(filter(listAdded[0]));//remove added files
					else
						classesCommit.add(filter(listDeleted[d]));
				}
			}
			else{
				if(i==0)
					continue;
				String[] listAdded=listClass[i].split("Added : ");
				if(listAdded.length>1)
					classesCommit.add(filter(listAdded[0]));//remove added files
				else
					classesCommit.add(filter(listClass[i]));
			}
		}
    	return classesCommit;
    }
	
	private String filter(String text){
		String [] filter=text.split("src");
		
		if(filter.length>1){
			filter= filter[filter.length-1].split("/java");
			if(filter.length>1)
				return filter[filter.length-1];
			else
				return filter[0];
		}else	
			return text;
		
	}	
		
	private ArrayList<String> designContent(){
		for(int index=0;index<this.listClassVersion.size();index++){
			putSubArrayToContent(this.listClassVersion.get(index));// write classes
			content.add(this.listSvnVersions.get(index)+"\t"+ "n� de bugs: \t "+this.listBugsVersions.get(index).size()+"\t"+ "n� de classes: \t "+this.listClassVersion.get(index).size()+"\t commitsBug: \t"+listIdCommitsBugsVersions.get(index).size());//write versions
			System.out.println(this.listClassVersion.get(index));
			System.out.println(listIdCommitsBugsVersions.get(index));
			System.out.println(listSvnVersions.get(index));
		}
		Collections.reverse(content);
		return this.content;		
	}
	
	private void putSubArrayToContent(ArrayList<?> subArray){//process  sub array
		for(int index=0;index<subArray.size();index++){
			//this.content.add(subArray.get(index).toString());			
		}		
	}
	
	public ArrayList<String> getContent(){
		return this.content;
	}
	
	public ArrayList<ArrayList<String>> getListClassVersion(){		
		return this.listClassVersion;
	}
	
	public ArrayList<String> getListSvnVersions(){
		return this.listSvnVersions;
	}
	
	
}