package br.ufba.dcc.mmcc.mapping;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public  class BadSmellsMapping {
	
	private ArrayList<ArrayList<ArrayList<String>>> badSmellsClassesVersion= new ArrayList<ArrayList<ArrayList<String>>>();
	//private ArrayList<ArrayList<String>> classesVersions=new ArrayList<ArrayList<String>>();
	private Map<String,Integer> mapIdBadSmell = new HashMap<String, Integer>();
	private ArrayList<ArrayList<String>> badSmellsClass= new ArrayList<ArrayList<String>>();
	private ArrayList<String> content= new ArrayList<String>();	
	BadSmellsMapping(File diretorio){
		definingBadSmellsMapping();
		readerFolder(diretorio);
		reverseArrayList(badSmellsClassesVersion);//set descending order
		designContent();
	}
	
	private void definingBadSmellsMapping(){
		mapIdBadSmell.put("dataclass.txt",0);
		mapIdBadSmell.put("dataclumps.txt",1);
		mapIdBadSmell.put("featureenvy.txt",2);
		mapIdBadSmell.put("godclass.txt",3);
		mapIdBadSmell.put("messagechains.txt",4);		
		mapIdBadSmell.put("scrizophrenicclass.txt",5);		
		mapIdBadSmell.put("traditionbreaker.txt",6);
	}		
	
	 private void readerFolder(File initialPath){
		 if (initialPath.exists()) {  
	            File[] contents = initialPath.listFiles();          
	            //System.out.println(contents.length);
	            for (int i=0; i<contents.length; i++) {  
	            	   if (contents[i].isFile()) { 
	            		   String path=contents[i].getPath();
	                    	IncodeFile incode= new IncodeFile(path);
	                    	int idBadsmell= mapIdBadSmell.get(contents[i].getName());
	                    	//System.out.println(contents[i]);
	                    	this.badSmellsClass.add(idBadsmell,incode.getClassesBadSmells());//add classes on bad smells
	                    	if(i==(contents.length-1)){
	                    		 badSmellsClassesVersion.add(badSmellsClass);//add bad smells by versions
	                    		 this.badSmellsClass=new ArrayList<ArrayList<String>>();
	                    	}
	            	   }
	            	   else{
	            		    Collections.sort(Arrays.asList(contents), new Comparator<File>() {
	       					@Override
		       					public int compare(File o1, File o2) {
		       						return Integer.valueOf(o1.getName()).compareTo(Integer.valueOf(o2.getName()));
		       					}
	       					});
	            		   readerFolder(contents[i]);
	            		   System.out.println(contents[i]);
	            	   }
	             }
		  }	
	 }		 
	
	 private ArrayList<String> designContent(){		 	
			for(int index=0;index<badSmellsClassesVersion.size();index++){
				putSubArrayToContent(badSmellsClassesVersion.get(index));// write classes
				content.add("version index:" +(index)+"\t");
			}
			reverseArrayList(content);
			return this.content;		
	}
	 
	 private void reverseArrayList(ArrayList<?> arrayList){
		 Collections.reverse(arrayList);
	 }
	 
	 private void putSubArrayToContent(ArrayList<?> subArray){//process  sub array
		 int total=0;
		 ArrayList<String> classesVersions= new ArrayList<String>();
			for(int index=0;index<subArray.size();index++){
				//content.add(subArray.get(index).toString());
				ArrayList<String> classes =(ArrayList<String>) subArray.get(index);
				//content.add("qtd classes bad smell:"+classes.size());				
				 for(int i=0;i<classes.size();i++){
					 if(!classesVersions.contains(classes.get(i))){
						 classesVersions.add(classes.get(i));
					 }
				 }
			}
			content.add("total classes na versao: \t "+classesVersions.size());
	 }
	 
	 private int noRepeatClasses(ArrayList<String> badsmells){
		 ArrayList<String> classesVersions= new ArrayList<String>();
		 for(int i=0;i<badsmells.size();i++){
			 if(!classesVersions.contains(badsmells.get(i))){
				 classesVersions.add(badsmells.get(i));
			 }
		 }
		 return classesVersions.size();
	 }
	 
	 public ArrayList<String> getContent(){
			return this.content;
	 }
	 
	 public ArrayList<ArrayList<ArrayList<String>>> getBadSmellsClassesVersion(){		
		 return badSmellsClassesVersion;
	 }
	 
	 public Map<String,Integer> getMapIdBadSmell(){
		 return mapIdBadSmell;
	 }	 
	
}
