package br.ufba.dcc.mmcc.mapping;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

public abstract class LeitorMetaModelo {

	protected String path;
	protected BufferedReader readFile;
	//BufferedReader readFile;
	//File file = new File(path);
	
	LeitorMetaModelo(String path){
		this.path=path;
		readFile();		
	}
	
	private void readFile(){
		 try {
		      FileReader file = new FileReader(this.path);
		      this.readFile = new BufferedReader(file);
		 }    
		 catch (IOException e) {
		        System.err.printf("Erro na abertura do arquivo: %s.\n",
		          e.getMessage());
		 }
		 
	}
	protected abstract void readLine();
	
		
	
	
	
}
