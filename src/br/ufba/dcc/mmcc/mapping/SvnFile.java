package br.ufba.dcc.mmcc.mapping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;


public class SvnFile extends LeitorMetaModelo {
	protected ArrayList<String> versions= new ArrayList<String>();	
	protected ArrayList<Integer> idCommitsVersions=new ArrayList<Integer>();;
	protected String key="Added : /tomcat/tc6.0.x/tags/";
	private int limitCommitVersion;
	private int maxVersions;
	private ArrayList<String> content=new ArrayList<String>();	
	
	
	SvnFile(String path,String keyword,int limitCommitVersion,int maxVersions) {
		super(path);
		this.maxVersions=maxVersions;
		this.key=keyword;
		this.limitCommitVersion=limitCommitVersion;
		readLine();
	}

	@Override
	protected void readLine() {
		String line = null;		
		
		try {
			line = readFile.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		while(line!=null && idCommitsVersions.size()<maxVersions){
			//System.out.printf("%s; ", line);
			String[] chunk = line.split("\t");
			//System.out.printf("%s %s;\n ", chunk[3],chunk[0]);
			int id=Integer.parseInt(chunk[0]);
			if(chunk[3].contains(this.key)){				
				if(!versions.contains(getNameVersions(chunk[3])) ){
					if(idCommitsVersions.size()>0){
						if((idCommitsVersions.get(idCommitsVersions.size()-1)-id)>limitCommitVersion){
							System.out.println(getNameVersions(chunk[3])+" - "+idCommitsVersions.size()+" \t"+(idCommitsVersions.get(idCommitsVersions.size()-1)-id));
							content.add(getNameVersions(chunk[3])+" \t"+(idCommitsVersions.get(idCommitsVersions.size()-1)-id)+"\t"+id);
							this.versions.add(getNameVersions(chunk[3]));
							this.idCommitsVersions.add(id);
						}
						
					}else{
						System.out.println(getNameVersions(chunk[3]));
						content.add(getNameVersions(chunk[3]));
						this.versions.add(getNameVersions(chunk[3]));
						this.idCommitsVersions.add(id);
					}
				}				
			}
			try {
				line = readFile.readLine();				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
	}
	
	public ArrayList<String> getversions(){		
		return versions;		
	}
	
	public ArrayList<Integer> getIdCommitsVersions(){		
		return idCommitsVersions;		
	}
	
	public ArrayList<String> getContent(){		
		Collections.reverse(content);
		return content;		
	}
	
	private String getNameVersions(String version){
		String [] chunk=version.split(key);
		version=chunk[1];
		chunk=chunk[1].split("/");
		if(chunk.length>0)
			version=chunk[0];
		version=version.replaceAll(" ","");
		return version;		
	}
	
	
}

