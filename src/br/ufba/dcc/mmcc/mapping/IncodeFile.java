package br.ufba.dcc.mmcc.mapping;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class IncodeFile extends LeitorMetaModelo{

	private ArrayList<String> classes= new ArrayList<String>();
	//private ArrayList<ArrayList<ArrayList<String>>> badSmellsClassesVersion= new ArrayList<ArrayList<ArrayList<String>>>();
	//private ArrayList<ArrayList<String>> classesVersion= new ArrayList<ArrayList<String>>();
	
	
	IncodeFile(String path) {
		super(path);
		readLine();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void readLine() {
		String line = null;
		String pacote=null;
		
		
		try {
			line = readFile.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		while(line!=null){			
			
			if(line.split("\t").length<2 ){
				if(line.contains("<")){
					pacote=line.replace("<","");
					pacote=line.replace(">","");
				}
				else if(line.contains(".")){
					pacote=line.replace(".","/");
				}
				else
					pacote=line.replace("\t","");
				
				//System.out.println(line); 
				pacote=pacote.replace("\t","");
				
			}
			else{
				String chunk[]=line.split("\t");
				
				this.classes.add(pacote+"/"+chunk[1]+".java");
			
				
			}
				
		   try {
				line = readFile.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//System.out.println(pacote);
		}
		
	}
	public ArrayList<String> getClassesBadSmells(){
		return this.classes;
	}
	public String getKindBadSmell(){
		Path p = Paths.get(path);
		String nameFile = p.getFileName().toString();
		return nameFile;
	}
	
}
