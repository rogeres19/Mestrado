package br.ufba.dcc.mmcc.mapping;

import java.io.File;
import java.io.IOException;


public class Main {

	public static void main(String[] args) throws IOException {		
		
		//Parametros para execucao do programa Tomcat 6
		String changelogPath="/Users/rogeres/Google Drive/mestrado atual/coleta-dados/Sistemas/TomCat6/ChangeLogs.txt";
		String versionKeyword="Added : /tomcat/tc6.0.x/tags/";
		String relinkResult="/Users/rogeres/Google Drive/mestrado atual/coleta-dados/Sistemas/TomCat6/final_results.txt";
		String badsmellFolderPath="/Users/rogeres/Google Drive/mestrado atual/coleta-dados/Sistemas/TomCat6/badsmells/";
		int limitCommitVersion=0;
		int maxVersions=100;
				
		SvnFile svn= new SvnFile(changelogPath,versionKeyword,limitCommitVersion,maxVersions);
		new PrinterMapping("outputMapping/versions_avaliable.txt", svn.getContent());
		RelinkFile relink= new RelinkFile(relinkResult);
		VersionMapping mapping= new VersionMapping(svn, relink);		
		String outputPath2="outputMapping/mapping_bad_smells_versions.txt";		
		PrinterMapping printer= new PrinterMapping("outputMapping/mapping_bugs_versions.txt", mapping.getContent());
		File diretorio = new File(badsmellFolderPath);
		BadSmellsMapping badSmellsMapping= new BadSmellsMapping(diretorio);
		new PrinterMapping(outputPath2, badSmellsMapping.getContent());
		Analize analize= new Analize(mapping, badSmellsMapping);		
		new PrinterMapping("outputMapping/Analize.txt",analize.getContent());
		
	}
	

}
