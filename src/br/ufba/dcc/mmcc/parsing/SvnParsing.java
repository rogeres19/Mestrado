package br.ufba.dcc.mmcc.parsing;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SvnParsing {
	public static void main(String argv[]) {

		try {
	
			String path_file="/Users/rogeres/Google Drive/mestrado atual/coleta-dados/Sistemas/Lenya/arquivo.xml";
			File fXmlFile = new File(path_file);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("logentry");
			System.out.println("----------------------------");
			Path file1 = Paths.get("ChangeLogs.txt");			
			ArrayList<String> linhas = new ArrayList<String>();
			//ArrayList<String> linhas2 = new ArrayList<String>();
			int tamanho=nList.getLength();
			//for (int temp = 0; temp < nList.getLength(); temp++) {
			for (int temp = 0; temp < tamanho; temp++) {
				Node nNode = nList.item(temp);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());
				StringBuffer sb = new StringBuffer();
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					sb.append(eElement.getAttribute("revision"));
					sb.append("\t");
					String date_creation_ts =eElement.getElementsByTagName("date").item(0).getTextContent();
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-d'T'HH:mm:s");
					Date date_creation= formatter.parse(date_creation_ts);
					long unixTime_creation_ts = (long)date_creation.getTime()/1000;
					sb.append(unixTime_creation_ts);
					sb.append("\t");
					String author;
					try{
						author= eElement.getElementsByTagName("author").item(0).getTextContent();
					}
					catch(NullPointerException e){
						author="null";
					}
					sb.append(author);
					sb.append("\t");
					String tempPath=eElement.getElementsByTagName("msg").item(0).getTextContent().replace("\n", " ");
					tempPath= tempPath.replace("\t"," ");
					sb.append(tempPath);
					int tamanhoPath;
					try{
						tamanhoPath= eElement.getElementsByTagName("path").getLength();
					}
					catch(NullPointerException e){
						tamanhoPath=-1;
					}
						for(int i=0; i<tamanhoPath;i++){
							Node tmpNode=eElement.getElementsByTagName("path").item(i);
							Element tmpElement= (Element) tmpNode;						
							String tmpText=tmpElement.getAttribute("action").replace("M"," Modified : ");
							tmpText=tmpText.replace("A","Added : ");
							tmpText=tmpText.replace("D","Deleted : ");
							tmpText = " "+tmpText + eElement.getElementsByTagName("path").item(i).getTextContent().replace("\n", " ");
							tmpText= tmpText.replace("\t"," ");
							
							sb.append(tmpText+"  ");
						}
				}				
				linhas.add(sb.toString());
				Files.write(file1, linhas, Charset.forName("UTF-8"));
			}
			System.out.println(nList.getLength());			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
