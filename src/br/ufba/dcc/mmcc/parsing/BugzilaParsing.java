package br.ufba.dcc.mmcc.parsing;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
/**
 * Essa classe ler arquivo xml do bugzilla e gera dois arquivos de entrada para ferramenta Relink
 * Arquivos de saida bugData.txt e bugCommentData.txt
 */
public class BugzilaParsing {

	public static void main(String argv[]) {

		try {
			//arquivo de entrada
			File fXmlFile = new File("/Users/rogeres/Google Drive/mestrado atual/coleta-dados/Sistemas/TomCat6/tomcat6.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			//ArrayList<ArrayList<String>>versao = new ArrayList<ArrayList<String>>();
			Map<String,Integer> mapa = new HashMap<String,Integer>();
			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
			
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("bug");
			System.out.println("----------------------------");
			Path file1 = Paths.get("outputParsing/bugData.txt");
			Path file2 = Paths.get("outputParsing/bugCommentData.txt");
			ArrayList<String> linhas = new ArrayList<String>();
			ArrayList<String> linhas2 = new ArrayList<String>();
			int tamanho=nList.getLength();
			for (int temp = 0; temp < tamanho; temp++){
				Node nNode = nList.item(temp);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());
				StringBuffer sb = new StringBuffer();
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String version =eElement.getElementsByTagName("version").item(0).getTextContent();
					if(version!=""){
						if(mapa.containsKey(version))
							mapa.put(version, mapa.get(version) + 1);
						else
							mapa.put(version, 1);
					}
					sb.append(eElement.getElementsByTagName("bug_id").item(0).getTextContent());
					sb.append("\t");
					sb.append("null" );
					sb.append("\t");
					sb.append(eElement.getElementsByTagName("bug_status").item(0).getTextContent());
					sb.append("\t");
					sb.append(eElement.getElementsByTagName("assigned_to").item(0).getTextContent());
					sb.append("\t");
					sb.append(eElement.getElementsByTagName("reporter").item(0).getTextContent());
					sb.append("\t");
					String date_creation_ts =eElement.getElementsByTagName("creation_ts").item(0).getTextContent();
					//String dateString="2009-07-09 22:48:00 +0000";
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-d HH:mm:s");
					Date date_creation= formatter.parse(date_creation_ts);
					long unixTime_creation_ts = (long)date_creation.getTime()/1000;
					sb.append(unixTime_creation_ts);
					sb.append("\t");
					String date_delta_ts =eElement.getElementsByTagName("delta_ts").item(0).getTextContent();
					Date date_delta= formatter.parse(date_delta_ts);
					long unixTime_delta_ts = (long)date_delta.getTime()/1000;
					sb.append(unixTime_delta_ts);
					sb.append("\t");
					sb.append("\t");
					sb.append(eElement.getElementsByTagName("short_desc").item(0).getTextContent());
					sb.append("\t");
					for(int i=0; i<eElement.getElementsByTagName("comment_count").getLength();i++){
						String tmpText = eElement.getElementsByTagName("thetext").item(i).getTextContent().replace("\n", " ");
						tmpText= tmpText.replace("\t"," ");
						sb.append(tmpText);
					}					
				}				
				linhas.add(sb.toString());
				Files.write(file1, linhas, Charset.forName("UTF-8"));
			}
			System.out.println(nList.getLength());			
			for (int temp = 0; temp < tamanho; temp++) {
				Node nNode = nList.item(temp);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());				
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					for(int i=0; i<eElement.getElementsByTagName("comment_count").getLength();i++){						
						StringBuffer sb = new StringBuffer();
						sb.append(eElement.getElementsByTagName("bug_id").item(0).getTextContent());
						sb.append("\t");
						Element eSubElement = (Element) nNode;
						sb.append(eSubElement.getElementsByTagName("who").item(i).getTextContent());
						sb.append("\t");
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-d HH:mm:s");
						String dateString =eSubElement.getElementsByTagName("bug_when").item(i).getTextContent();
						Date date= formatter.parse(dateString);
						long unixTime_bug_when= (long)date.getTime()/1000;
						sb.append(unixTime_bug_when);
						sb.append("\t");
						String tmpText = eSubElement.getElementsByTagName("thetext").item(i).getTextContent().replace("\n", " ");
						tmpText= tmpText.replace("\t","--");
						sb.append(tmpText);
						linhas2.add(sb.toString());
					}					
					Files.write(file2, linhas2, Charset.forName("UTF-8"));
				}				
			}
			System.out.println(Arrays.asList(mapa));
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}