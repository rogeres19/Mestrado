![#mapBsB](img/mapbsb.png) 
==========

## O que � ?
-------------
� um conjunto de scripts para suporte ao mapeamento de Bad smells e bugs em classes e vers�es de um sistema.

## O que faz?
-------------
#### MapBsB � um conjunto de scripts que realiza: 

* Parsing de arquivo de log do SVN em arquivo de entrada Relink;
* Parsing de arquivo de Relat�rio de Bugs do Bugzilla em arquivo de entrada da Relink;
* Mapeia Bugs em classes e vers�es do sistema a partir do changelog e de arquivo de commits e bugs pareados da ferramenta Relink;
* Associa classes com Bad smells com classes que tiveram ocorr�ncia de bugs;
* Quantifica ocorr�ncia de bugs para cada vers�o do sistema;
* Quantifica classes com bugs para os bad smells do estudo (God class, Feature envy, Schizophrenic class, Data clumps, Data class, Message chains e Tradition breaker);
* Quantifica classes com bad smells por vers�o do sistema.

## Pr�-requisitos
-----------------
- Requer arquivo de log SVN;
- Requer Arquivo de Relat�rio de bugs do Bugzilla;
- Requer ferramenta para detec��o de bad smells;
- Requer uso de ferramenta Relink para associar bugs com commits;

## Como funciona?
---------------
* Para obter arquivos de entrada Relink - executar BugzillaParsing informando localiza��o do arquivo xml do Bugzilla, e executar SvnParsing informando caminho para o arquivo de log do svn;
* Para realizar mapeamentos de bad smells e bugs em classes e vers�es do sistema, dentre outras fun��es, � necess�rio executar a classe Main com as seguinte informa��es:
   - changelogPath="caminho de arquivo ChangeLogs.txt";
   -  String versionKeyword=" Palavra-chave de indica��o de tag.";
   -  String relinkResult="arquivo gerado pela Relink (final_results.txt)";
   -  String badsmellFolderPath="caminho para pasta de arquivos com mapeamento de classes com bad smells";
   - int limitCommitVersion= n�mero m�nimo de commits entre vers�es;
   - int maxVersions= n�mero m�ximo de vers�es para serem analisadas; 

## Contribuinte:

* [**Rogeres Santos do Nascimento**](http://gitlab.com/rogeres19): Estudante de Mestrado da Universidade Federal da Bahia (UFBA).